set(LIBAPP_SOURCES
  macroDefinitions.h
  readCommandLine.h
  sfDirections.cpp
  sfUtility.cpp
  structures.h
  themeSelector.h
  userOptions.h
  readCommandLine.cpp
  sfDirections.h
  sfUtility.h
  themeSelector.cpp
  userOptions.cpp
)

add_library(appobj OBJECT ${LIBAPP_SOURCES})
target_include_directories(appobj PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/..)
