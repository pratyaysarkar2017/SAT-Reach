set(LIBCORE_SOURCES
  continuous/approxModel/approxModel.cpp
  continuous/approxModel/approxModel.h
  continuous/approxModel/fbInterpol.cpp
  continuous/approxModel/fbInterpol.h
  continuous/polytope/polytope.cpp
  continuous/polytope/polytope.h
  continuous/convexSet/createPolytopeFromConvexSet.cpp 
  continuous/convexSet/createPolytopeFromConvexSet.h
  continuous/convexSet/supportFunctionProvider.h
  continuous/convexSet/transMinkPoly.cpp
  continuous/convexSet/transMinkPoly.h
  discrete/discreteSet/discreteSet.cpp
  discrete/discreteSet/discreteSet.h
  hybridAutomata/hybridAutomata.cpp
  hybridAutomata/hybridAutomata.h
  hybridAutomata/location.cpp
  hybridAutomata/location.h
  hybridAutomata/transition.cpp
  hybridAutomata/transition.h
  hybridAutomata/varToIndexMap.cpp
  hybridAutomata/varToIndexMap.h
  math/2dGeometry.cpp
  math/analyticOdeSol.cpp
  math/glpkLpSolver/glpkLpSolver.cpp
  math/glpkLpSolver/glpkLpSolver.h
  math/matrix.cpp
  math/matrixExponential.hpp
  math/numeric/approxComparator.cpp
  math/numeric/approxComparator.h
  math/numeric/basicGeometry.h
  math/numeric/comp.h
  math/numeric/tribool.h
  math/r8lib.cpp
  math/2dGeometry.h
  math/analyticODESol.h
  math/lpSolver/lpSolver.cpp
  math/lpSolver/lpSolver.h
  math/matrixExponential.cpp
  math/matrix.h
  math/point/point.h
  math/point/point.cpp
  math/r8lib.hpp
  reachability/postCSequential.cpp
  reachability/postCSequential.h
  reachability/reachability.cpp
  reachability/reachability.h
  symbolicStates/initialState.cpp
  symbolicStates/initialState.h
  symbolicStates/symbolicStates.cpp
  symbolicStates/symbolicStates.h
  symbolicStates/symbolicStatesUtility.cpp
  symbolicStates/symbolicStatesUtility.h
  
)

add_library(coreobj OBJECT ${LIBCORE_SOURCES})
target_include_directories(coreobj PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/..)



