
#include <core/symbolicStates/symbolicStatesUtility.h>

symbolic_states::ptr searchSymbolic_state(
		std::list<symbolic_states::ptr> symbolic_list,
		symbolic_states::ptr symbolic) {
	symbolic_states::ptr found_symbolic_state;
	auto iter = find(symbolic_list.begin(),symbolic_list.end(),symbolic);
	if(iter != symbolic_list.end())
		return *iter;
	else{
		std::cout<<"Symbolic state search:: Not Found!\n";
		return nullptr;
	}
}
